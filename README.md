# README #

Automation using java and selenium webdriver

### What is this repository for? ###

Framework demonstrate following features:
1. Spring dependency injection
2. Singleton pattern to manage single webdriver instance and global waut properties(Fluent/implicit)
3. Maven using maven-compiler plugin for1.8 and cuke/web/guava/common dependencies
4. Cucumber using cucumber junit & cucumber options e.g. glue,-dryrun and tags.
5. Factory pattern to increase reusability of common components (e.g. Topic in Page and Post)
6. Builder pattern to manage navigation and flexibility to build steps for tester using "." operator
7. Tags to run multiple Scenarios
8. Property file for reusable element,env,url,database property
9. Combine all library in page object model and behavior in command builders
10. Custom exception handling using user define exceptions

### How do I get set up? ###

create a clone and use it as Maven project.
It can be enhance by spring and more abstraction in framework.

### Contribution guidelines ###

Writing tests
Code review
