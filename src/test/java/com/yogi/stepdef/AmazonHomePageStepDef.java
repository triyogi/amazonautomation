package com.yogi.stepdef;

import com.yogi.common.BaseStepDef;
import com.yogi.pageobjects.AmazonHomePage;
import com.yogi.pageobjects.ItemDetailPage;
import com.yogi.pageobjects.ShoppingCartPage;
import com.yogi.selenium.Driver;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class AmazonHomePageStepDef extends BaseStepDef{
    @Autowired
    AmazonHomePage amazonHomePage;
    @Autowired
    ItemDetailPage itemDetailPage;
    @Autowired
    ShoppingCartPage shoppingCartPage;

    @Given("^I am on amazon home page$")
    public void i_am_on_amazon_home_page() throws Throwable {
        Driver.getDriver();
    }

    @Given("^I search for \"([^\"]*)\" book$")
    public void i_search_for_book(String book) throws Throwable {
        amazonHomePage.goTo().SearchItem(book);
    }

    @Given("^I add the first book to my cart$")
    public void i_add_the_first_book_to_my_cart() throws Throwable {
        itemDetailPage.goTo().SelectItme();
        itemDetailPage.goTo().AddItemInCart();
    }

    @Given("^I see the message \"([^\"]*)\" with all details$")
    public void i_see_the_message_with_all_details(String messageText) throws Throwable {
        itemDetailPage.goTo().VerifyMessageForAddedItem(messageText);
    }

    @When("^I check my shopping cart$")
    public void i_check_my_shopping_cart() throws Throwable {
        shoppingCartPage.goTo().ItemInTheShoppingCart();
    }

    @Then("^I should see the book in my shopping cart$")
    public void i_should_see_the_book_in_my_shopping_cart() throws Throwable {
        Assert.assertTrue(shoppingCartPage.goTo().VerifyItemIsAddedToTheCart());
    }

    @After
    public void tearDown(){
        Driver.closeWebBrowser();
    }
}
