package com.yogi.testrunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"classpath:cucumber/"}
        ,glue = {"classpath:"}
        ,tags = "@AdddItem"
)
public class CucumberRunner {
}
