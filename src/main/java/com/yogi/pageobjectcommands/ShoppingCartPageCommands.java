package com.yogi.pageobjectcommands;

import com.yogi.exceptions.ItemNotAdded;
import com.yogi.selenium.Driver;
import org.openqa.selenium.By;

public class ShoppingCartPageCommands {

    boolean verification=false;

    public void ItemInTheShoppingCart() {
        Driver.getDriver().findElement(By.id("nav-cart")).click();
    }

    public boolean VerifyItemIsAddedToTheCart() throws ItemNotAdded {
        if(Driver.getDriver().findElement(By.linkText("Harry Potter: The Complete 8-Film Collection")).isEnabled()){
            verification=true;
        }else {
            throw new ItemNotAdded("Item is not added to the cart");
        }
        return verification;
    }
}
