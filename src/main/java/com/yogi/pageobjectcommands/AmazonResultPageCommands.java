package com.yogi.pageobjectcommands;

import com.yogi.selenium.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class AmazonResultPageCommands {

    public void SearchItem(String book) {
        Driver.getDriver().findElement(By.id("twotabsearchtextbox")).sendKeys(book);
        Driver.getDriver().findElement(By.id("twotabsearchtextbox")).sendKeys(Keys.ENTER);
    }
}
