package com.yogi.pageobjectcommands;

import com.yogi.exceptions.ItemIsNotExistOnPage;
import com.yogi.exceptions.ItemNotAdded;
import com.yogi.selenium.Driver;
import org.openqa.selenium.By;

public class ItemDetailPageCommands {

    public static void SelectItme() throws ItemIsNotExistOnPage {
        if(Driver.getDriver().findElement(By.linkText("Harry Potter: The Complete 8-Film Collection")).isEnabled()){
            Driver.getDriver().findElement(By.linkText("Harry Potter: The Complete 8-Film Collection")).click();
        }
        else {
            throw new ItemIsNotExistOnPage("Item is not exist on the page");
        }
    }

    public void AddItemInCart(){
        Driver.getDriver().findElement(By.id("add-to-cart-button")).click();
    }

    public void VerifyMessageForAddedItem(String messageText) throws ItemNotAdded {
        String messageTextValue= Driver.getDriver().findElement(By.xpath("//*[@id=\"huc-v2-order-row-confirm-text\"]/h1")).getText();
        if(messageTextValue.contains(messageText))
        {
        }else
        {
            throw new ItemNotAdded("Item has not been added to the cart");
        }
    }

}
