package com.yogi.springconfig;

import com.yogi.pageobjects.AmazonHomePage;
import com.yogi.pageobjects.ItemDetailPage;
import com.yogi.pageobjects.ShoppingCartPage;
import org.springframework.context.annotation.Bean;

/**
 * Created by YOGI on 17/09/2017.
 */
public class TestPageObjectsConfig {
    @Bean
    public AmazonHomePage amazonHomePage(){
        return new AmazonHomePage();
    }

    @Bean
    public ItemDetailPage itemDetailPage(){
        return new ItemDetailPage();
    }

    @Bean
    public ShoppingCartPage shoppingCartPage(){
        return new ShoppingCartPage();
    }
}
