package com.yogi.common;

import com.yogi.springconfig.TestPageObjectsConfig;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by YOGI on 17/09/2017.
 */
@ContextConfiguration(classes = {TestPageObjectsConfig.class})
public class BaseStepDef {
}
